﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int health = 5;
    //enemy has 5 health on start

    public void TakeDamage (int damageAmount)
    {
        health -= damageAmount;

        if (health <=0)
        {
            Destroy(gameObject);
        }
    }

    //added enemy health and death once health reaches 0
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
