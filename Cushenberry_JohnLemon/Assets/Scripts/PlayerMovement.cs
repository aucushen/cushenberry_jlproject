﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public float turnSpeed = 20f;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;
    public Transform hitBox;
    public float speed;
    bool onGround = true;


    public int health = 100;
    public void TakeDamage(int damageAmount)

    {
        health -= damageAmount;

        if (health <= 0)
        {
            Destroy(gameObject);

        }
    }


    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();



    }



    private void Update()
    {
        onGround = Physics.Raycast(transform.position, Vector3.down, .52f);

        if (Input.GetKeyDown(KeyCode.F)) //f key will speed up JL adding this feature created an error, JL now has inertia
        {
            speed = speed * 6f;
        }
        if (Input.GetKeyUp(KeyCode.F))
        {
            speed = speed / 6f;
        }

        if (Input.GetKeyDown(KeyCode.L))// l key will shrink JL
        {
            transform.localScale = Vector3.one * .5f;
            Invoke("ReturnToFullSize", 5f);//Return back to size called
        }


        if (Input.GetKeyDown(KeyCode.Space))//Space bar to attack enemies
        {

            Collider[] hitColliders = Physics.OverlapBox(hitBox.transform.position, hitBox.transform.localScale, hitBox.transform.rotation);

            for (int i = 0; i < hitColliders.Length; i++)
            {
                if (hitColliders[i].gameObject.CompareTag("Enemy"))
                {
                    EnemyHealth eHealth = hitColliders[i].gameObject.GetComponent<EnemyHealth>();

                    if (eHealth != null)
                        eHealth.TakeDamage(1);
                    //localscale/2f***Physics.OverLapBox




                }

            }

        }
    }

    void ReturnToFullSize()

    {
        transform.localScale = Vector3.one; //return size func 

    }

    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(horizontal, 0.0f, vertical);

        m_Rigidbody.AddForce(movement * speed);

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Speed"))
        {
            other.gameObject.SetActive(false);
        }
    }
}



